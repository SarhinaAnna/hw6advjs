async function getIP() {
  let response = await fetch("https://api.ipify.org/?format=json");
  let ip = await response.json();
  console.log(ip);
  let adressa = await fetch("http://ip-api.com/json/92.217.88.55?lang=en");
  let adres = await adressa.json();
  console.log(adres);

  document.querySelector(`.button`).insertAdjacentHTML(
    "afterend",
    `<div class="ip">
    
                    <div ><b>${adres.timezone} </b></div>
                    <div ><b>${adres.country}</b></div>
                    <div ><b>${adres.region}</b></div>
                    <div ><b>${adres.city}</b></div>
                    <div ><b>${adres.regionName}</b></div>
                    </div>
                    `
  );
}

document.querySelector(`.button`).addEventListener("click", function (event) {
  getIP();
});
